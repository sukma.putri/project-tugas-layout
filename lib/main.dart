import 'dart:ui';
import 'package:flutter/material.dart';
void main() {
 runApp(MyApp());
}
class MyApp extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return MaterialApp(
 home: Scaffold(
 appBar: AppBar(
 backgroundColor: Colors.black87,
 title: Text('PROFIL'),
 centerTitle: true,
 leading: Icon(
 Icons.menu,
 ),
 ),
 body: Center(
 child: Column(
 children: <Widget>[
 Picture(),
 TextName(),
 FirstRow(),
 SecondRow(),
 ],
 ),
 ),
 ),
 );
 }
}
class Picture extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return Container(
 width: 200,
 height: 200,
 margin: const EdgeInsets.only(top: 10.0),
 decoration: BoxDecoration(
 borderRadius: BorderRadius.circular(100.100),
 image: DecorationImage(
 image: AssetImage('assets/IMG_2546.JPG'),
 fit: BoxFit.cover,
 )),
 );
 }
}
class TextName extends StatelessWidget {
 @override Widget build(BuildContext context) {
 return Container(
  //margin: const EdgeInsets.only(bottom: 250.0),
 child: Text(
 "Sukma Putri Novianti",
 style: TextStyle(
 fontSize: 24,
 color: Colors.black,
 ),
 ),
 );
 }
}
class FirstRow extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return Row(
 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
 children: <Widget>[
 Container(
 decoration: BoxDecoration(boxShadow: [
 BoxShadow(
 color: Colors.blueGrey,
 blurRadius: 10.0,
 spreadRadius: 1.0,
 )
 ]),
 width: 150,
 //margin: const EdgeInsets.only(bottom:2550.0),
 child: Card(
 child: Padding(
 padding: EdgeInsets.all(18.0),
 child: Column(
 children: <Widget>[
 Icon(
 Icons.school,
size: 50,
color: Colors.blue,
 ),
Text(
 'Undiksha',
 style: TextStyle(color: Colors.black, fontSize: 18),
 )
 ],
 ),
 ),
 ),
 ),
 Container(
 decoration: BoxDecoration(
 boxShadow: [
 BoxShadow(
 color: Colors.blueGrey,
 blurRadius: 10.0,
 spreadRadius: 1.0,
 ), ],
 ),
 width: 150,
 //margin: const EdgeInsets.only(bottom: 550.0),
 child: Card(
 child: Padding(
 padding: EdgeInsets.all(18.0),
 child: Column(
 children: <Widget>[
 Icon(
 Icons.location_city,
size: 50,
color: Colors.red,
 ),
Text(
 'Singaraja',
style: TextStyle(color: Colors.black, fontSize: 18),
 )
 ],
 ),
 ),
 ),
 ),
 ],
 );
 }
}
class SecondRow extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
 return Row(
 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
 children: <Widget>[
 Container(
 decoration: BoxDecoration(boxShadow: [
 BoxShadow(
 color: Colors.blueGrey,
 blurRadius: 10.0,
 spreadRadius: 1.0,
 )
 ]),
 width: 150,
 //margin: const EdgeInsets.only(bottom:550.0),
 child: Card(
 child: Padding(
 padding: EdgeInsets.all(18.0),
 child: Column(
 children: <Widget>[
 Icon(
 Icons.sports_esports,
size: 50,
color: Colors.green,
 ),
Text(
 'All Genre',
style: TextStyle(color: Colors.black, fontSize: 18), )
 ],
 ),
 ),
 ),
 ),
 Container(
 decoration: BoxDecoration(
 boxShadow: [
 BoxShadow(
 color: Colors.blueGrey,
 blurRadius: 10.0,
 spreadRadius: 1.0,
 ),
 ],
 ),
 width: 150,
 //margin: const EdgeInsets.only(bottom: 550.0),
 child: Card(
 child: Padding(
 padding: EdgeInsets.all(18.0),
 child: Column(
 children: <Widget>[
 Icon(
 Icons.home,
size: 50,
color: Colors.yellow,
 ),
 Text(
 'Gerokgak',
style: TextStyle(color: Colors.black, fontSize: 18),
 )
 ],
 ),
 ),
 ),
 )
 ],
 );
 }
}